<?php

/*
Plugin Name: Fixed Cart Coupon Override
Plugin URI: https://www.majemedia.com/plugins/fixed-cart-coupon-override
Description: Overrides the default coupon type on the create coupon page
Version: 1.1
Author: Maje Media LLC
Author URI: https://www.majemedia.com/plugins/fixed-cart-coupon-override
Copyright: 2019
Text Domain: fixedcartcouponoverride
Domain Path: /lang
*/

if( ! defined( 'ABSPATH' ) ) {
	die();
}

class FixedCartCouponOverride {

	private static $instance;
	public         $plugin_url;
	public         $plugin_path;

	public static function get_instance() {

		if( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function __construct() {

		$this->set_class_vars();

		$this->hooks();

	}

	public function set_class_vars() {

		$this->plugin_path = realpath( dirname( __FILE__ ) );
		$this->plugin_url  = plugins_url( '', __FILE__ );

	}

	public function hooks() {

		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_coupon_override' ] );
		add_filter( 'woocommerce_get_sections_products', [ $this, 'set_default_coupon_type_section' ] );
		add_filter( 'woocommerce_get_settings_products', [ $this, 'set_default_coupon_type_settings' ], 10, 2 );

	}

	public function set_default_coupon_type_section( $sections ) {

		$sections[ 'fixedcartcouponoverride' ] = __( 'Override Default Coupon Type', 'fixedcartcouponoverride' );

		return $sections;

	}

	public function set_default_coupon_type_settings( $settings, $current_section ) {

		if( $current_section !== 'fixedcartcouponoverride' ) {
			return $settings;
		}

		$settings_select = [
			[
				'name' => __( 'Fixed Cart Coupon Override', 'fixedcartcouponoverride' ),
				'type' => 'title',
				'desc' => __( 'Set the default coupon type', 'fixedcartcouponoverride' ),
				'id'   => 'fixedcartcouponoverridetitle',
			],
			[
				'name'    => 'Default coupon type',
				'id'      => 'fixedcartcouponoverridesetting',
				'type'    => 'select',
				'options' => wc_get_coupon_types(),
			],
			[
				'type' => 'sectionend',
				'id'   => 'fixedcartcouponoverride',
			],
		];

		return $settings_select;

	}

	public function enqueue_coupon_override() {

		global $current_screen;

		if( empty( $current_screen ) ) {
			return;
		}

		if( $current_screen->id !== 'shop_coupon' ) {
			return;
		}

		if( $current_screen->action !== 'add' ) {
			return;
		}

		$override_default = get_option( 'fixedcartcouponoverridesetting', '' );

		// bailing if it's set to fixed_cart cause that's already the default.
		if( empty( $override_default ) || $override_default === 'fixed_cart' ) {
			return;
		}

		wp_enqueue_script( 'fixedcartcouponoverride', $this->plugin_url . '/assets/js/shop_coupon_type_override.js', [ 'jquery' ], '1.0', FALSE );
		wp_localize_script( 'fixedcartcouponoverride', 'fixedcartcouponoverride', [ 'value' => $override_default ] );

	}

}

$FixedCartCouponOverride = FixedCartCouponOverride::get_instance();